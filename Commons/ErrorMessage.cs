﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace C_Sharp_Keijiban.Commons
{
    public class ErrorMessage
    {
        public const string LOGIN_FAILED = "ログインに失敗しました";
        public const string USER_DUPLICATE = "ユーザー情報が重複しています";
        public const string ACCOUNT_DUPLICATE = "アカウント名が重複しています";
        public const string EMAIL_DUPLICATE = "メールアドレスが重複しています";

        public const string MESSAGE_NOT_ENTERED = "つぶやきを入力してください";
        public const string ACCOUNT_NOT_ENTERED = "アカウント名を入力してください";
        public const string PASSWORD_NOT_ENTERED = "パスワードを入力してください";

        public const string MESSAGE_LIMIT = "つぶやきは140文字以下で入力してください";
        public const string NAME_LIMIT = "名前は20文字以下で入力してください";

        public const string ACCOUNT_NOT_MATCHES = "アカウント名は半角英数字6文字以上20文字以下で入力してください";
        public const string PASSWORD_NOT_MATCHES = "パスワードは半角6文字以上20文字以下で入力してください";
        public const string EMAIL_NOT_MATCHES = "メールアドレスは半角50文字以下で入力してください";
    }
}